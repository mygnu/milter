#![doc(hidden)]

use std::sync::atomic::{AtomicBool, Ordering};

// Callback panics must be handled *somehow*, since panicking across an FFI
// boundary is undefined behaviour in Rust. On panic, this library calls
// libmilter’s `smfi_stop` function, and since that is a one-way, terminal
// operation, it seems fine to also use a tripwire in the form of this static
// Boolean flag here. Worker threads are outside our control, that is why we
// have to prevent callback code from executing by using a flag.
static PANICKED: AtomicBool = AtomicBool::new(false);

// Private API, not for public use.
pub fn set_panicked(panicked: bool) {
    PANICKED.store(panicked, Ordering::Relaxed);
}

// Private API, not for public use.
pub fn is_panicked() -> bool {
    PANICKED.load(Ordering::Relaxed)
}
