conn = mt.connect("inet:3334@localhost")
assert(conn, "could not open connection")

local err = mt.conninfo(conn, "client.example.com", "123.123.123.123")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.helo(conn, "mail.example.com")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.mailfrom(conn, "from@example.com")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.rcptto(conn, "to@example.com")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

-- TODO mt.data always times out. This is a bug in miltertest, see:
-- https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=946386
-- local err = mt.data(conn)
-- assert(err == nil, err)
-- assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.header(conn, "Test-Name", "Test-Value")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.eoh(conn)
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.bodystring(conn, "body")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.eom(conn)
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.disconnect(conn)
assert(err == nil, err)
